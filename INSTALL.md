How to Install vdev
===================

This document is meant to help both packagers and individuals wishing
to install `vdev` from source to do so as painlessly as possible.
However, the reader is expected to have basic command-line
proficiency, familiarity with their bootloader of choice, and
familiarity with installing and removing system services.

The steps in this document are lightly tested, and are **specific to
Devuan**. Please report bugs to the Devuan mailing list.

Step 1: Set up the system and build packages
--------------------------------------------

    # apt-get install git build-essential dh-make dpkg-dev
    # git clone git@git.devuan.org:ralph.ronnquist/vdev
    # cd vdev
    # make deb

Note that, if it makes sense to you, you can use the alternative
command "`make jail`" instead, to have the building deposit files into
a `firejail` overlay, instead of cluttering up the workspace.

Step 2: Install packages and set up initramfs
---------------------------------------------

**NOTE:** This will disable `udev`. If `vdev` does not work correctly
for you, you are expected to know how to restore `udev`. See below.

    # dpkg -i *.deb

Note that the installation runs the (`vdev`) script
`/usr/share/vdev/vdev-install.sh vdev` for making the choice of using
`vdev` without uninstalling `udev`, apart from removing `udev` scripts
for the `initramfs` building. You can use the same script with either
`udev` or `vdev` as argument, to change back to using `udev` or
`vdev`. This will rebuild the `initramfs` each time.

Step 3: Uninstall vdev
----------------------

The following is possibly the safest way to unistall `vdev`:

    # /usr/share/vdev/vdev-install.sh udev
    # apt-get purge vdev-initramfs vdevd libudev1-compat
    # apt-get install --reinstall initramfs-tools udev
