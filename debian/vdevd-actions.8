.TH VDEVD-ACTIONS 8 "August 28, 2016"
.SH NAME
vdevd-actions \- action definitions for the \fIvdevd\fR device-file manager daemon
.SH DESCRIPTION

\fBvdevd\fR is a portable userspace device-file manager daemon. Its
main task is to manage the "/dev" file structue in response to kernel
events, to add, remove and configure device nodes and symbolic links
so to make devices available through the traditional well defined
naming scheme.

The details of operation are programmed by a combination of
\fIaction\fR declarations and \fIhelper\fR scripts. The action
declarations are used for recognising the types of devices, and
mapping these to appropriate initialization actions. The helper
scripts implement these actions.

.SH ACTION SYNTAX
An action file consists of a single declaration block headed by a line
saying "[vdev-action]". This is followed by a number of assignment
patterns to be matched on the event, as well as result assignments for
the \fIhelperf\R variable, which identifies the assoicated helper
script, and the \fIdaemonlet\fR variable, which signals whether or not
to invoke the helper script via the \fIdaemonlet\fR wrapper script.

.HP
For example, the \fI001-sysfs-block.act\fR action looks as follows:
.br
[vdev-action]
.br
event=add
.br
type=block
.br
OS_DEVPATH=
.br
helper=sysfs.sh
.br
daemonlet=true
.P
The action matches for add events of block devices where OS_DEVPATH is
empty, and as a result, the helper script \fIsysfs.sh\fR is invoked
via the \fIdaemonlet\fR wrapper script.
.P
Specifically: unless stated otherwise, all action fields are optional,
and all fields must match the device event for the action's command to
be executed.
.IP \fBevent\fR 2
The \fIevent\fR field is *required. Use "add" to match a device-add
event, "remove" to match a device-remove event, "change" to match a
device-change event (such as a battery level changing), or "any" to
match any device event.
.IP \fBpath\fR 2
The \fIpath\fR field is optional. This is an extended POSIX regular
expression that matches the kernel-given device path (such as \fIsda\R
or \fIinput/mice\fR) to the action.
.IP \fBtype\fR 2
The \fItype\fR field is optional. This is either "block" or "char",
which will match the event only if it it is for a block or character
device, respectively. Not all devices are representable by block or
character device files, however, which is why this field is optional.
.IP \fBOS_*\fR 2
\fIOS_*\fR fields are optional. Any field prefixed with \fIOS_\fR
matches an OS-specific device attribute. On Linux, these include the
names of \fIuevent\fR fields. For example, an action can match a
device's subsystem using the \fIOS_SUBSYSTEM\fR field. If an empty
value is given (e.g. "OS_SUBSYSTEM="), then the action will match any
value as long as the device attribute is present in the device event
(for example, a Linux \fIuevent\fR packet that does not have
\fISUBSYSTEM\fR defined will not match an action with
"OS_SUBSYSTEM=").
.P
Once a device event is encountered that matches all of an action's
fields, \fIvdevd\fR runs the commands specified by the action's ini
file. The fields that describe the commands are:
.IP \fBrename_command\fR 2
This is a shell command to run to generate the path to the device file
to create. It will be evaluated after the device event matches the
action, but before the device file is created. The command must write
the desired path to stdout. Its output will be truncated at 4,096
characters (which is \fIPATH_MAX\fR on most filesystems). Note that
not all devices have paths given by the kernel.
.IP \fBcommand\fR 2
This is the shell command to run once the device file has been
successfully created. Unless specified otherwise, \fIvdevd\fR will run
this as a synchronous subprocess; i.e., it will wait until the command
has completed before processing the next action.
.IP \fBasync\fR 2
If set to "true", this tells \fIvdevd\fR to start and run the action
\fIcommand\fR asynchronously with its continued processing, i.e., to
not wait until the commmand has completed. This is useful for
long-running device setup tasks.
.IP \fBif_exists\fR 2
Commands are executed optimistically; even if one fails, the other
subsequent ones will be attempted, unless the device already exists
and \fIvdevd\v\fR encounters \fIif_exists=error\fR in one of the
matching actions. If the device already exists (given by the exists
flag), then only run commands with \fIif_exists=run\fR.
.IP \fBdaemonlet\fR 2
If set to "true", this tells \fIvdevd\fR to run the command with a
\fIdaemonlet\fR wrapping. See "Advanced Device Handling" below for a
description of what this means.
.P
NOTE: \fIvdevd\fR matches a device event against actions according to
their files' lexographic order. Moreover, it processes device events
sequentially, in the order in which they arrive. This is also true for
the Linux port; the kernel's SEQNUM field is passed on to actions, but
not used for sequencing the event handling.

\fIvdevd\fR communicates device information to action commands using
environment variables. When a \fIcommand\fR or \fIrename_command\fR
runs, the following environment variables will be set:
.IP \fB$VDEV_ACTION\fR 2
This is the \fIevent\fR field of the action, i.e. "add", "remove",
"change", or "any".
.IP \fB$VDEV_DAEMONLET\fR 2
If set to 1, then the command is being invoked as a daemonlet (see the
"Advanced Device Handling" section below).
.IP \fB$VDEV_CONFIG_FILE\fR 2
This is the absolute path to the configuration file \fIvdevd\fR is
using.
.IP \fB$VDEV_HELPERS\fR 2
This is the absolute path to the directory containing \fIvdevd\fR's
helper programs.
.IP \fB$VDEV_INSTANCE\fR 2
This is a randomly-generated string that corresponds to this running
intance of \fIvdevd\fR. It will be different on each invocation of
\fIvdevd\fR.
.IP \fB$VDEV_GLOBAL_METADATA\fR 2
The absolute path to the metadata directory into which \fIvdevd\fR
writes metadata (e.g. \fI/dev/metadata\fR).
.IP \fB$VDEV_LOGFILE\fR 2
If specified, this is the path to \fIvdevd\fR's logfile.
.IP \fB$VDEV_MAJOR\fR 2
If the device event corresponds to a block or character device, this
is the device file's major number.
.IP \fB$VDEV_METADATA\fR 2
If the device is given a path (either by the kernel or the
\fIrename_command\fR output), this is the absolute path to the
directory to contain the device's metadata.
.IP \fB$VDEV_MINOR\fR 2
If the device event corresponds to a block or character device, this
is the device file's minor number.
.IP \fB$VDEV_MODE\fR 2
If the device event corresponds to a block or character device, this
is the string "block" or "char" (respectively).
.IP \fBI$VDEV_MOUNTPOINT\fR 2
This is the absolute path to the directory in which the device files
will be created. For example, this is usually \fI/dev\fR.
.IP \fB$VDEV_OS_*\fR 2
Any OS-specific device attributes will be encoded by name, prefixed
with \fIVDEV_OS_\fR. For example, on Linux, the SUBSYSTEM field in the
device's \fIuevent\fR packet will be exported as
\fI$VDEV_OS_SUBSYSTEM\fR. Similarly, the DEVPATH field in a device's
\fIuevent\fR packet will be exported as \fI$VDEV_OS_DEVPATH\fR.
.IP \fB$VDEV_PATH\fR 2
If the device is given a path (either by the kernel or the
\fIrename_command\fR output), this is the path to the device file
*relative* to \fI$VDEV_MOUNTPOINT\fR. The absolute path to the device
file can be found by \fI$VDEV_MOUNTPOINT/$VDEV_PATH\fR. Note that not
all devices have device files (such as batteries, PCI buses, etc.).

.SH Advanced Device Handling
.P
By default, \fIvdevd\fR will \fIfork()\fR and \fIexec()\fR the
\fIcommand\fR for each device request in the system shell. However, as
an optimization, \fIvdevd\fR can run the command as a *daemonlet*.
This means that the \fIcommand\fR will be expected to stay resident
once it proceses a device request, and receive and process subsequent
device requests from \fIvdevd\fR instead of simply exiting after each
one. Doing so saves \fIvdevd\fR from having to \fIfork()\fR and
\fIexec()\fR the same command over and over again for common types of
devices, and lets it avoid having to repeatly incur long \fIcommand\fR
start-up times. It also allows the administrator to define stateful or
long-running actions that can work on sets of devices.
.P
The programming model for daemonlet commands is as follows:
.IP 1. 3
\fIvdevd\fR will \fIfork()\fR and \fIexec()\fR the command directly.
It will *not* invoke the system shell to run it.
.IP 2. 3
\fIvdevd\fR will write a sequence of newline-terminated strings to the
command's \fIstdin\fR, followed by an empty newline string. These
strings encode the request's environment variables, and are in the
form \fINAME=VALUE\\n\fR.
.IP 3. 3
The \fIcommand\fR is expected to write an ASCII-encoded exit code to
\fIstdout\fR to indicate the success/failure of processing the
request. 0 indicates success, and non-zero indicates failure.

If the \fIcommand\fR crashes or misbehaves, \fIvdevd\fR will log as such
and attempt to restart it.

.SH LIST OF ACTIONS
\fIvdevd\fR includes the following list of actions:
.IP
.br
/etc/vdev/actions/001-sysfs-block.act
.br
/etc/vdev/actions/001-sysfs-char.act
.br
/etc/vdev/actions/002-modalias.act
.br
/etc/vdev/actions/003-firmware.act
.br
/etc/vdev/actions/agpgart.act
.br
/etc/vdev/actions/block.act
.br
/etc/vdev/actions/btrfs-control.act
.br
/etc/vdev/actions/char.act
.br
/etc/vdev/actions/cpu.act
.br
/etc/vdev/actions/cpu-msr.act
.br
/etc/vdev/actions/cuse.act
.br
/etc/vdev/actions/device-mapper.act
.br
/etc/vdev/actions/dialout.act
.br
/etc/vdev/actions/disk.act
.br
/etc/vdev/actions/disk-part.act
.br
/etc/vdev/actions/dm-disk.act
.br
/etc/vdev/actions/dri.act
.br
/etc/vdev/actions/err.act
.br
/etc/vdev/actions/fb.act
.br
/etc/vdev/actions/full.act
.br
/etc/vdev/actions/fuse.act
.br
/etc/vdev/actions/fuse-mounts.act
.br
/etc/vdev/actions/hwdb.act
.br
/etc/vdev/actions/ifname.act
.br
/etc/vdev/actions/input.act
.br
/etc/vdev/actions/irlpt.act
.br
/etc/vdev/actions/kmsg.act
.br
/etc/vdev/actions/kvm.act
.br
/etc/vdev/actions/legousbtower.act
.br
/etc/vdev/actions/lircN.act
.br
/etc/vdev/actions/loop.act
.br
/etc/vdev/actions/loop-control.act
.br
/etc/vdev/actions/lp.act
.br
/etc/vdev/actions/mem.act
.br
/etc/vdev/actions/mISDNtimer.act
.br
/etc/vdev/actions/mmtimer.act
.br
/etc/vdev/actions/mwave.act
.br
/etc/vdev/actions/netdev.act
.br
/etc/vdev/actions/net-tun.act
.br
/etc/vdev/actions/null.act
.br
/etc/vdev/actions/nvram.act
.br
/etc/vdev/actions/optical.act
.br
/etc/vdev/actions/parport.act
.br
/etc/vdev/actions/pktcdvd.act
.br
/etc/vdev/actions/pktcdvdN.act
.br
/etc/vdev/actions/port.act
.br
/etc/vdev/actions/power-switch.act
.br
/etc/vdev/actions/printer.act
.br
/etc/vdev/actions/ptmx.act
.br
/etc/vdev/actions/qftN.act
.br
/etc/vdev/actions/random.act
.br
/etc/vdev/actions/raw.act
.br
/etc/vdev/actions/rawctl.act
.br
/etc/vdev/actions/rfkill.act
.br
/etc/vdev/actions/rtc.act
.br
/etc/vdev/actions/sch.act
.br
/etc/vdev/actions/sclp_line.act
.br
/etc/vdev/actions/sgi.act
.br
/etc/vdev/actions/sonypi.act
.br
/etc/vdev/actions/sound-alsa-control.act
.br
/etc/vdev/actions/sound-audio.act
.br
/etc/vdev/actions/tty3270.act
.br
/etc/vdev/actions/tty.act
.br
/etc/vdev/actions/ttyN.act
.br
/etc/vdev/actions/ttyS.act
.br
/etc/vdev/actions/ttysclp.act
.br
/etc/vdev/actions/usb-bus.act
.br
/etc/vdev/actions/v4l.act
.br
/etc/vdev/actions/vcs.act
.br
/etc/vdev/actions/virtio.act
.br
/etc/vdev/actions/virtualbox-usb.act
.br
/etc/vdev/actions/virtualbox-usb_device.act
.br
/etc/vdev/actions/z90crypt.act
.br
/etc/vdev/actions/zzz-event-push.act
.br
/etc/vdev/actions/zzz-udev-compat.act
.\".SH ACCESS CONTROL LISTS
.SH HELPERS
\fIvdevd\fR includes the following helper scripts and binaries:
.IP \fBblock.sh\fR 2
Helper script to link a block device to /dev/block/$MAJOR:$MINOR.
.IP \fBchar.sh\fR 2
Helper sript to link a character device to /dev/char/$MAJOR:$MINOR.
.IP \fBdaemonlet\fR 2
This is a wrapper script around persistent helper scripts.
\fIdameonlet\fR provides a loop for successive helper script
invocation, in a loop that first reads ASCII-encoded KEY=VALUE
environment variables until it receives "done" (indicating the end of
a stanza), then invokes the helper script, and then prints the return
code.
.br
If reading an empty string, \fIdaemonlet\fR exits the loop.
.IP \fBdev-setup.sh\fR 2
This script is not meant to be run manually.
.IP \fBdisk.sh\fR 2
Helper for setting up symlinks to disks and their partitions.
.IP \fBdm-disk.sh\fR 2
Helper script for generating \fIdm\fR block device paths and symlinks.
.IP \fBecho_n\fR 2
Helper binary that merely implements "echo -n", i.e. to print to
stdout all its arguments separated by space, and no terminating
newline.
.IP \fBevent-push.sh\fR 2
Helper script to push device events to listening processes. The event
to push is defined by environment variables: $VDEV_OS_SEQNUM,
$VDEV_OS_DEVPATH, $VDEV_OS_SUBSYSTEM, and the optional $VDEV_PATH.
.br
The event is pushed vie \fIevent-put\fR to the socket
\fI/dev/events/global\fR.
.IP \fBevent-put\fR 2
Helper binary to trigger an event from an stdin event definition. Run
it as "event-put -h" to get usage information.
.IP \fBfirmware.sh\fR 2
This is a helper script for firmware loading.
.IP \fBgen-ifnames.sh\fR 2
Helper script that merely filters the output of "ip link show" into a
series of "$IFNAME mac $MAC" lines for the available network
interfaces.
.IP \fBhwdb-props.sh\fR 2
Helper script to insert a device's hardware database files.
.IP \fBhwdb.sh\fR 2
Helper script to query the hardware database (if it exists), and
extract useful properties from it. NOTE: does not use vdev's subr.sh;
this program can run independently.
.IP \fBifname.sh\fR 2
Helper script to rename interfaces to have persistnet names, according
to the rules in $VDEV_IFNAMES_PATH The format of this file should be
lines of: "$IFNAME $ID_TYPE $ID_ARG" where $IFNAME is the persistent
name; $ID_TYPE is either "mac" or "devpath", and $ID_ARG is either the
MAC address (formatted xx:xx:xx:xx:xx:xx) if $ID_TYPE is "mac", or
$ID_ARG is the device path (starting with /devices)
.IP \fBinput.sh\fR 2
Helper script to set up /dev/input/by-id and /dev/input/by-path.
.IP \fBmodprobe.sh\fR 2
Helper script to invoke modprobe with $VDEV_OS_MODALIAS argument.
.IP \fBnetdev.sh\fR 2
Helper script to collate properties for a network device.
.IP \fBoptical.sh\fR 2
Helper script to set up symlinks to optical (CD, DVD) devices, based
on capability.
.IP \fBpermissions.sh\fR 2
Helper script to set permissions and ownership. It takes the owner
from the VDEV_VAR_PERMISSIONS_OWNER environment variable, the group
from the VDEV_VAR_PERMISSIONS_GROUP environment variable and/or the
mode from the VDEV_VAR_PERMISSIONS_MODE environment variable.
.IP \fBpower-switch.sh\fR 2
Helper script to tag "power switch" devices as such in the metadata.
.IP \fBsound.sh\fR 2
Helper script to set up /dev/snd/by-path and /dev/snd/by-id.
.IP \fBstat_ata\fR 2
Helper binary to read product/serial number for ATA drives.
.IP \fBstat_bus\fR 2
Helper binary to set up bus link(s) according to sub system.
.IP \fBstat_input\fR 2
Helper binary to read input device properties.
.IP \fBstat_net\fR 2
Helper binary to read network device properties, given an interface.
.IP \fBstat_optical\fR 2
Helper binary to get drive capabilities from an open device node,
using the Linux-specific CDROM_GET_CAPABILITY ioctl.
.IP \fBstat_path\fR 2
Helper binary to build a persistent path name from sysfs.
.IP \fBstat_scsi\fR 2
Helper binary to read SCSI device properties.
.IP \fBstat_usb\fR 2
Helper binary to read USB device properties.
.IP \fBstat_v4l\fR 2
Helper binary to read video4linux device properties.
.IP \fBsubr-event.sh \fR 2
Helper script inclusion file that defines a collection of utility
functions for event processing, to be used in helper scripts.
.IP \fBsubr-hwdb.sh \fR 2
Helper script inclusion file that defines a collection of utility
functions for hardware database access, to be used in helper scripts.
.IP \fBsubr.sh \fR 2
Helper script inclusion file that defines a collection of more generic
utility functions, to be used in helper scripts.
.IP \fBsysfs.sh\fR 2
Helper script to store device metadata from sysfs to the OS metadata
database. In particular, map a device's sysfs path to its metadata
directory.
.IP \fBudev-compat.sh\fR 2
Helper script to maintain \fIudev\fR compatibility.
.IP \fBusb-name.sh\fR 2
Helper script to echo USB device details when known.
.IP \fBv4l.sh\fR 2
Helper script for video4linux devices.
.IP \fBVBoxCreateUSBNode.sh\fR 2
Helper script for creating and removing device nodes for VirtualBox
USB devices.

.SH SEE ALSO
.BR vdevd (8).

.SH CONTRIBUTIONS

This software was primarily developed by
Jude Nelson <judecn@gmail.com>.
.br
Various contributions by
Didier Kryn <kryn@in2p3.fr>,
Kylie McClain <somasis@exherbo.org>,
Jack Frost <fbt@fleshless.org,
tilt12345678,
Suedi,
Obarun,
Peter Gossner <gossner@internode.on.net>
.br
This man page is maintained by Ralph Ronnquist
<ralph.ronnquist@gmail.com>. Most of the detailed content is authored
by Jude Nelson, then edited and formatted into this form by Ralph.


