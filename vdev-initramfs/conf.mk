# This make file defines the vdevd runtime configuration. It uses
# relative pathnames from here. This makefile is used in
# ../vdevd/Makefile for installing the configuration when vdevd is
# installed.
#

include ../buildconf.mk

all: conf-install

OVERRIDES_BUILD := $(BUILD_ETCDIR)/insserv/overrides/vdev
OVERRIDES_INSTALL := $(DESTDIR)$(ETCDIR)/insserv/overrides/vdev

INITSCRIPT_BUILD := $(BUILD_VDEV_INITSCRIPT)/vdev
INITSCRIPT_INSTALL := $(INSTALL_VDEV_INITSCRIPT)/vdev

ACTIONS := $(wildcard actions/*.act)

ACTIONS_BUILD := $(patsubst actions/%.act,$(BUILD_VDEV_CONFIG)/actions/%.act,$(ACTIONS))
ACTIONS_INSTALL := $(patsubst actions/%.act,$(INSTALL_VDEV_CONFIG)/actions/%.act,$(ACTIONS))

ACLS := $(wildcard acls/00-whitelist-root.acl)
ACLS_BUILD := $(patsubst accls/%.acl,$(BUILD_VDEV_CONFIG)/acls/%.acl,$(ACLS))
ACLS_INSTALL := $(patsubst acls/%.acl,$(INSTALL_VDEV_CONFIG)/acls/%.acl,$(ACLS))

IFNAMES_BUILD := $(BUILD_VDEV_CONFIG)/ifnames.conf
IFNAMES_INSTALL := $(INSTALL_VDEV_CONFIG)/ifnames.conf

CONF_BUILD := $(BUILD_VDEV_CONFIG)/vdevd.conf
CONF_INSTALL := $(INSTALL_VDEV_CONFIG)/vdevd.conf

VDEVD_CONF_BUILD := $(CONF_BUILD) $(IFNAMES_BUILD) $(ACLS_BUILD) \
	$(ACTIONS_BUILD) $(INITSCRIPT_BUILD) $(OVERRIDES_BUILD)

VDEVD_CONF_INSTALL := $(CONF_INSTALL) $(IFNAMES_INSTALL) $(ACLS_INSTALL) \
	$(ACTIONS_INSTALL) $(INITSCRIPT_INSTALL) $(OVERRIDES_INSTALL)

$(OVERRIDES_BUILD): insserv-overrides-udev
	@mkdir -p $(shell dirname "$@")
	@cp -a "$<" "$@"

$(OVERRIDES_INSTALL): $(OVERRIDES_BUILD)
	@mkdir -p $(shell dirname "$@")
	@cp -a "$<" "$@"

$(INITSCRIPT_BUILD): sysv-initscript.sh 
	@mkdir -p $(shell dirname "$@")
	@cp -a "$<" "$@"

$(INITSCRIPT_INSTALL): $(INITSCRIPT_BUILD)
	@mkdir -p $(shell dirname "$@")
	@cp -a "$<" "$@"

$(CONF_BUILD): vdevd.conf.in
	@mkdir -p $(shell dirname "$@")
	@cat "$<" | \
		sed -e 's~@PREFIX@~$(PREFIX)~g;' \
		    -e 's~@CONF_DIR@~$(ETCDIR_VDEV)~g;' \
		    -e 's~@RUN_DIR@~$(RUNDIR_VDEV)~g;' \
		    -e 's~@LOG_DIR@~$(LOGDIR_VDEV)~g;' > "$@"

$(IFNAMES_BUILD): ifnames.conf.in 
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"
	$(GEN_IFNAMES) >> "$@"

$(CONF_INSTALL): $(CONF_BUILD)
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

$(IFNAMES_INSTALL): $(IFNAMES_BUILD)
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

$(BUILD_VDEV_CONFIG)/actions/%: actions/%
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

$(INSTALL_VDEV_CONFIG)/actions/%: $(BUILD_VDEV_CONFIG)/actions/%
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

$(BUILD_VDEV_CONFIG)/acls/%: acls/%
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

$(INSTALL_VDEV_CONFIG)/acls/%: $(BUILD_VDEV_CONFIG)/acls/%
	@mkdir -p $(shell dirname "$@")
	@rm -f "$@"
	@cat "$<" > "$@"

.phony: conf-install
conf-install: $(VDEVD_CONF_INSTALL)
