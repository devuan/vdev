include buildconf.mk

.PHONY: all
all:
	$(MAKE) -C vdevd
	$(MAKE) -C libudev-compat
#	$(MAKE) -C fs
	$(MAKE) -C vdev-initramfs
	$(MAKE) -C hwdb

.PHONY: install
install:
	$(MAKE) -C vdevd install
	$(MAKE) -C libudev-compat install
#	$(MAKE) -C fs install
	$(MAKE) -C vdev-initramfs install
	$(MAKE) -C hwdb install

.PHONY: clean
clean:
	$(MAKE) -C vdevd clean
	$(MAKE) -C libudev-compat clean
#	$(MAKE) -C fs clean
	$(MAKE) -C vdev-initramfs clean
	$(MAKE) -C hwdb clean

.PHONY: uninstall
uninstall:
	$(MAKE) -C vdevd uninstall
	$(MAKE) -C libudev-compat uninstall
#	$(MAKE) -C fs uninstall
	$(MAKE) -C vdev-initramfs uninstall
	$(MAKE) -C hwdb uninstall

.PHONY: deb jail
deb:
	make -f debian.mk

jail:
	firejail --overlay make -f debian.mk
