
LIC=lgpl
EMAIL=ralph.ronnquist@gmail.com
VERSION=1.0
ARCH=$(shell uname -r)
HOMEPAGE =

DEB = vdev_$(VERSION)_$(ARCH).deb

all: $(DEB)

$(DEB):
	#dh_make -a -s -n -y -e "$(EMAIL)" -p vdev_$(VERSION) -c $(LIC)
	PREFIX= INCLUDE_PREFIX=/usr dpkg-buildpackage -us -uc
