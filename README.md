Devuan packaging of vdev - a device-file manager and filesystem
===============================================================

NOTE: this project is a fork of [Jude Nelson's vdev
project](https://git.devuan.org/unsystemd/vdev), which at this point
is an orphaned project. This project is aimed at the preparation of
Devuan packaging.

The components are:

> `libudev1-compat`, which is the library ABI-compatible with
  libudev 219.

> `vdevd`, which is the hotplug daemon, including helpers, actions and
  configuration;

> `vdev-initramfs`, which provides an initramfs-tools intergration.
  This package includes a script (/usr/share/vdev/vdev-install.sh) for
  making a manual choice between using vdev or udev as /dev manager.


Installing vdev will disable any prior `udev`, without unsinstalling
it. However, `vdev` competes with `udev` on three fronts:
  + the library link `libudev-so.1`
  + the scripts for the initrd, and
  + the sysvinit `/etc/rcS.d` scripts.

The current solution deals with these aspects underneath and invisibly
for the Devuan (Debian) package management, rather than straight out
replacing `udev` and `libudev1`. You can, whenever, alternate between
`udev` and `vdev` via (in a terminal):

    # /usr/share/vdev/vdev-install.sh udev
or

    # /usr/share/vdev/vdev-install.sh vdev

It's probably **safest to always shift to `udev` before installing**
other things. I don't think it's vital, but there are many possible
failure cases.

Other Notes
-----------

Please refer to [Jude Nelson's vdev
project](https://git.devuan.org/unsystemd/vdev) for an extensive
description of the software.
