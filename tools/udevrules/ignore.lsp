(define Ignore:Ignore nil)

(map (fn (x) (Ignore x true))
     (clean null? (parse (or (read-file IGNOREFILE) "") "\n")))

; 
(define (ignore-rule? rule)
  (unless (string? rule) (Ignore (lookup "LINE" rule))))

(define (add-ignore-rule rule)
  (Ignore (lookup "LINE" rule) true))

(define (save-ignore)
  (write-file IGNOREFILE (join (map first (Ignore)) "\n" true)))
